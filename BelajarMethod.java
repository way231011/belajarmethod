public class BelajarMethod {

        static void iniMethod () {
            // void tidak memiliki nilai balikaan atau return
            System.out.println("ini method");
        }

        static String iniString () {
            // jika method nya string maka balikan atau return harus string juga
            String nama = "Alvaro";
            System.out.println(nama);
            return nama;
        }

        static int menghitung () {
            int a = 10;
            System.out.println(a);
            return a;
        }

        public static void main (String[]args){
            iniMethod();
            iniString();
            menghitung();
        }
    }

