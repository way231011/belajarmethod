public class BelajarMethodPakaiParameter {
    public static void main(String[] args) {
        System.out.println(penjumlahan(10,10));
        System.out.println(penggabunganKata("Dimas","Bekasi"));
        iniParameterVoid("Alvaro",19,"Bekasi",165.5);
    }

    static int penjumlahan(int a,int b){
        return a + b;
    }

    static String penggabunganKata(String nama,String alamat){
        return nama + " " + alamat;
    }

    static void iniParameterVoid(String nama,int umur,String alamat,double tinggiBadan){
        System.out.println(nama + " " + umur + " " + alamat + " " + tinggiBadan + "cm");
    }
    }

